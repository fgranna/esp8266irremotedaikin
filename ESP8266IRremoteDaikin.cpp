#include "ESP8266IRremoteDaikin.h"


// # of bytes per command
const int COMMAND_LENGTH = 27;    

unsigned char daikinHeader[8] = {
  0x11,0xDA,0x27,0x00,0xC5,0x00,0x00,0xD7
};

unsigned char daikin[COMMAND_LENGTH]     = { 
0x11,0xDA,0x27,0xF0,0x00,0x00,0x00,0x20,
//0    1    2   3    4    5     6   7
0x11,0xDA,0x27,0x00,0x00,0x41,0x1E,0x00,
//8    9   10   11   12    13   14   15
0xB0,0x00,0x00,0x00,0x00,0x00,0x00,0xC1,0x80,0x00,0xE3 };
//16  17    18  19   20    21   22  23   24   25   26
//0xB0,0x00,0x00,0x00,0x00,0x00,0x00,0xC0,0x00,0x00,0xE3 };
//
//0x30 = 1 = 0011 0000
//0x40 - 2 = 0100 0000
//0x50 = 3 = 0101 0000
//0x60 = 4 = 0110 0000
//0x70 = 5 = 0111 0000
//0xa0 - AU= 1010 0000
//0xb0 = NI= 1011 0000
//
static byte vFanTable[] = { 0x30,0x40,0x50,0x60,0x70,0xa0,0xb0};




//0 AUTO, 1 FAN, 2 COOL, 3 DRY
//000 - AUT - 0x0 - 0000
//010 = DRY = 0x2 = 0010
//110 = COO = 0x3 = 0011
//001 = HEA = 0x4 = 0100
//011 = FAN = 0x6 = 0110
static byte vModeTable[] = {0x0, 0x2, 0x3, 0x4, 0x6};
//
// void IRdaikin()
// {
//  daikinController_off();
//  daikinController_setMode(0);
//  daikinController_setTemp(26);
//  daikinController_setFan(5);
//  daikin_setSwing_off();
// }

void print8Bit(uint8_t data) {
  int reversed = 0;
  for ( int b=0 ; b < 8 ; b++ ) reversed = ( reversed << 1 ) | ( 0x01 & ( data >> b ) );
  Serial.print(reversed, BIN);
  Serial.print("-");
  Serial.println(data, BIN);
}

IRDaikinSend irsend;

void ESP8266IRremoteDaikin::daikin_on()
{
  daikinController_on();
}  

void ESP8266IRremoteDaikin::daikin_off()
{
  daikinController_off();
}

void ESP8266IRremoteDaikin::daikin_verticalSwing(bool status)
{
  if (status) {
    daikin[16] |=0x0f;
  } else {
    daikin[16] &=0xf0;
  }
  daikinController_checksum();
}

void ESP8266IRremoteDaikin::daikin_horizontalSwing(bool status)
{
  if (status) {
    daikin[17] |=0x0f;
  } else {
    daikin[17] &=0xf0;
  }
  daikinController_checksum();
}

void ESP8266IRremoteDaikin::daikin_setMode(int mode)
{
  if (mode>=0 && mode <=4)
  {
    daikinController_setMode(vModeTable[mode]);   
  }
}

// 0~4 speed,5 auto,6 moon
void ESP8266IRremoteDaikin::daikin_setFan(int speed)
{
  if (speed>=0 && speed <=6)
  {
    daikinController_setFan(vFanTable[speed]);
  }
}

void ESP8266IRremoteDaikin::daikin_setTemp(int temp)
{
  if (temp >= 18 && temp<=32)
  {
    daikin[14] = (temp)*2;
    daikinController_checksum();
  }
}

void ESP8266IRremoteDaikin::daikin_sendCommand()
{
  sendDaikinCommand();
}

uint8_t ESP8266IRremoteDaikin::daikinController_checksum()
{
  uint8_t sum = 0;
  uint8_t i;

  for(i = 0; i <= 6; i++){
    sum += daikin[i];
  }
  daikin[7] = sum &0xFF;
  sum=0;
  for(i = 8; i <= 25; i++){
    sum += daikin[i];
  }
  daikin[26] = sum &0xFF;
}

void ESP8266IRremoteDaikin::dump()
{
  uint8_t i;
  for (i=0; i <COMMAND_LENGTH; i++) {
    Serial.print(daikin[i],HEX);
    Serial.print("-");
  }
}

//private function

void ESP8266IRremoteDaikin::daikinController_on()
{
  daikin[13] |= 0x01;
  daikinController_checksum();
}

void ESP8266IRremoteDaikin::daikinController_off()
{
  daikin[13] &= 0xFE;
  daikinController_checksum();
}

void ESP8266IRremoteDaikin::daikinController_setTemp(uint8_t temp)
{
  daikin[14] = (temp)*2;
  daikinController_checksum();
}


void ESP8266IRremoteDaikin::daikinController_setFan(uint8_t fan)
{
  daikin[16] &= 0x0f;
  daikin[16] |= fan;
  daikinController_checksum();
}

uint8_t ESP8266IRremoteDaikin::daikinController_getState()
{
  return (daikin[13])&0x01;
}

void ESP8266IRremoteDaikin::daikinController_setMode(uint8_t mode)
{
  daikin[13]=mode<<4 | daikinController_getState();
  daikinController_checksum();
}

void ESP8266IRremoteDaikin::sendDaikinCommand()
{
  irsend.sendDaikinWake();
  delay(20);
  irsend.sendDaikin(daikinHeader, 9,0); 
  delay(29);
  daikinController_checksum();  
  irsend.sendDaikin(daikin, 8,0); 
  delay(29);
  irsend.sendDaikin(daikin, 19,8); 
}

void ESP8266IRremoteDaikin::setPin(int pin)
{
  irsend.setPin(pin);
}
