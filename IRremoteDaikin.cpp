/*
 * IRremote
 * Version 0.11 August, 2009
 * Copyright 2009 Ken Shirriff
 * For details, see http://arcfn.com/2009/08/multi-protocol-infrared-remote-library.html
 *
 * Modified by Paul Stoffregen <paul@pjrc.com> to support other boards and timers
 * Modified  by Mitra Ardron <mitra@mitra.biz> 
 * Added Sanyo and Mitsubishi controllers
 * Modified Sony to spot the repeat codes that some Sony's send
 *
 * Interrupt code based on NECIRrcv by Joe Knapp
 * http://www.arduino.cc/cgi-bin/yabb2/YaBB.pl?num=1210243556
 * Also influenced by http://zovirl.com/2008/11/12/building-a-universal-remote-with-an-arduino/
 *
 * JVC and Panasonic protocol added by Kristian Lauszus (Thanks to zenwheel and other people at the original blog post)
 * LG added by Darryl Smith (based on the JVC protocol)
 */

#include "IRremoteDaikin.h"
#include "IRremoteIntDaikin.h"


void IRDaikinSend::sendDaikin(unsigned char buf[], int len, int start)
{
  int data2;
  enableIROut(38);
   
  mark(DAIKIN_HDR_MARK);
  space(DAIKIN_HDR_SPACE);
     
  for (int i = start; i < start+len; i++) {
    data2=buf[i];
    for (int j = 0; j < 8; j++) {
      if ((1 << j & data2)) {
        mark(DAIKIN_ONE_MARK);
        space(DAIKIN_ONE_SPACE);
      } else {
        mark(DAIKIN_ZERO_MARK);
        space(DAIKIN_ZERO_SPACE);
      }
    }
  }
  mark(DAIKIN_ONE_MARK);
  space(DAIKIN_ZERO_SPACE);
}

void IRDaikinSend::sendDaikinWake() {
  enableIROut(38);

  space(DAIKIN_ZERO_MARK);
  //
  mark(DAIKIN_ZERO_MARK);
  space(DAIKIN_ZERO_MARK);
  //
  mark(DAIKIN_ZERO_MARK);
  space(DAIKIN_ZERO_MARK);
  //
  mark(DAIKIN_ZERO_MARK);
  space(DAIKIN_ZERO_MARK);
  //
  mark(DAIKIN_ZERO_MARK);
  space(DAIKIN_ZERO_MARK);
  //
  mark(DAIKIN_ZERO_MARK);
  space(DAIKIN_ZERO_MARK);
}

void IRDaikinSend::sendRaw(unsigned int buf[], int len, int hz)
{
  enableIROut(hz);
  for (int i = 0; i < len; i++) {
    if (i & 1) {
      space(buf[i]);
    } 
    else {
      mark(buf[i]);
    }
  }
  space(0); // Just to be sure
}

void IRDaikinSend::setPin(int pin) {
  pinMode(IRpin, OUTPUT);
  //digitalWrite(IRpin, LOW); // When not sending PWM, we want it low  
  IRpin = pin;
}

void IRDaikinSend::mark(int time) {
  // Sends an IR mark for the specified number of microseconds.
  // The mark output is modulated at the PWM frequency.
  long beginning = micros();
  while(micros() - beginning < time){
    digitalWrite(IRpin, HIGH);
    delayMicroseconds(halfPeriodicTime);
    digitalWrite(IRpin, LOW);
    delayMicroseconds(halfPeriodicTime); //38 kHz -> T = 26.31 microsec (periodic time), half of it is 13
  }
}

/* Leave pin off for time (given in microseconds) */
void IRDaikinSend::space(int time) {
  // Sends an IR space for the specified number of microseconds.
  // A space is no output, so the PWM output is disabled.
  digitalWrite(IRpin, LOW);
  if (time > 0) delayMicroseconds(time);
}


void IRDaikinSend::enableIROut(int khz) {
  // Enables IR output.  The khz value controls the modulation frequency in kilohertz.
  halfPeriodicTime = 500/khz; // T = 1/f but we need T/2 in microsecond and f is in kHz

}
